import React, { useState } from 'react';
import Form from './components/form';
import Table from './components/table';
import PriceChart from './components/chart';



function App() {
  const [companyData, setData] = useState([])
  
  
  const onSubmit = data => {
    console.log(data);

    let api = `https://www.quandl.com/api/v3/datasets/WIKI/${data.symbol}.json?order=asc&start_date=${data.start}&end_date=${data.end}`

    fetch(api)
    .then(res => res.json())
    .then(data => {        
      setData(data.dataset.data) 

    }).catch(err => console.log(err))      
  }

  return (
    <div className="App container">
      <div className="row">      
        <Form handleRequest={onSubmit} />      
        <Table data={companyData} />      
       <PriceChart data={companyData} />  
          
      </div>
  </div>
  );
}

export default App;
