import React from 'react';
import { useForm } from 'react-hook-form';
import companies from './../companies.json'


function Form(props) {
    const { register, handleSubmit, errors } = useForm();

    const onSubmit = data => {
          return props.handleRequest(data)
    }
          
      
    
    return(
        <form onSubmit={ handleSubmit(onSubmit) } className="form-inline mx-auto mt-2 mb-3">
        <div className="form-group">
     
      <label>Select Company Symbol:
        <select name="symbol" ref={register({ required: true})} className="form-control">
          { companies.map((company, index) => <option key={ index }>{ company.Symbol }</option>
          )}
        </select>
      </label>       
       
        <input type="date" name="start" className="form-control" ref={register({ required: true})} />        
        <input type="date" name="end" className="form-control" ref={register({ required: true})} />
        <input type="email" name="email" className="form-control" ref={register({ required: true})} />
        
        <button type="submit" className="btn btn-primary">Search</button>
        </div>
      </form>
    )
}

export default Form;
