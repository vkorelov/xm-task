import React from 'react';


function Table(props) {
   
    let data = props.data

    
    return(
        
        data.length > 0 && (<table className="table table-dark">
        <thead>
          <tr>           
            <th>Date</th>
            <th>Open</th>
            <th>Hight</th>
            <th>Low</th>
            <th>Close</th>
            <th>Volume</th>
          </tr>
        </thead>
        <tbody>
          { props.data.map((row, index) => <tr>
          { row.slice(0,6).map( (value, index) => <td key={ index }>{ value }</td>) }
          </tr>)}
        </tbody>

      </table>)
    )
}

export default Table;